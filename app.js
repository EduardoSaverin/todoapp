var express = require('express');
var app = express();
var todoController = require("./controllers/todoController");

//set view engine
app.set("view engine","ejs");

//Fire Todo Controller
todoController(app,express.Router());

//static files
app.use(express.static("./public"));
app.listen(3000);
console.log("Listening on port 3000");
