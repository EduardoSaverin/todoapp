var bodyParser = require("body-parser");
var mongoose = require('mongoose');

//Connecting to Database
//Algorithm SCRAM-SHA-1 for MongoDB > 3.0
mongoose.connect('mongodb://localhost:27017/todoApp?authMechanism=SCRAM-SHA-1&authenticationDatabase=admin').catch(function(err){
   console.log(err);
});

//Create a Schema for todo-list
var todoSchema = new mongoose.Schema({
    item : String
});

//Create a Model for Schema
//Mongoose makes everything plural means it adds (s) to the end of model.
var todos = mongoose.model('todo',todoSchema);
/*
var item = todos({item : 'Turn on laptop'}).save(function(err){
   if(!err) console.log('Success');
});
*/

//var data = [];
//console.log(data);
//This is middleware
var urlEncodedParser = bodyParser.urlencoded({extended:true});
module.exports = function(app,router){
    app.use(bodyParser.json());
    app.use(urlEncodedParser);
    //Handle GET Requests
    app.get('/todo', function (req, res) {
        //Get all Todo Items From MongoDB And Pass it to the view.
        todos.find({}, function (err, data) {
            if (err) throw err;
            res.render('todo', {todos: data});
        });
    });
    //Handle POST Requests
    app.post('/todo', urlEncodedParser, function (req, res) {
        //data.unshift(req.body);
        var newTodo = todos(req.body).save(function (err, data) {
            if (err) throw err;
            res.send(JSON.stringify(data));
        });
        //console.log(req.body);
    });
    //Handle DELETE Requests
    app.delete('/todo/:item', function (req, res) {
        todos.find({item: req.params.item.replace(/\-/g, ' ')}).remove(function (err, data) {
            if (err) throw err;
            res.end(JSON.stringify(data));
        });
        /*data = data.filter(function(todo){
         return todo.item.replace(/ /g,'-') !== req.params.item;
         });*/

    });
}